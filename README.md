**BEFORE FILING AN ISSUE, READ THE RELEVANT SECTION IN THE [CONTRIBUTING]() FILE!!!**

Banana 3DS
==============
[![GitHub Actions Build Status]()]()
[![Discord]()](https://discord.gg/gREFmVW9cz)

Banana 3DS is an experimental open-source Nintendo 3DS emulator/debugger written in C++. It is written with portability in mind, with builds actively maintained for Windows, Linux and macOS.

Banana 3DS emulates a subset of 3DS hardware and therefore is useful for running/debugging homebrew applications, and it is also able to run many commercial games! Some of these do not run at a playable state, but we are working every day to advance the project forward. (Playable here means compatibility of at least "Okay" on our [game compatibility list]().)

Banana 3DS is licensed under the GPLv2 (or any later version). Refer to the license.txt file included. Please read the [FAQ](https://citra-emu.org/wiki/faq/) before getting started with the project.

Check out our [website]()!

Need help? Check out our [asking for help]() guide.

For development discussion, please join us on our [Discord server].

### Releases

Banana 3DS has two main release channels: Nightly and Canary.

The (https://gitlab.com/Gerold55/banana_3ds) build is based on the master branch, and contains already reviewed and tested features.

Both builds can be installed with the installer provided on the [website](), but those looking for specific versions or standalone releases can find them in the release tabs of the [Nightly](https://gitlab.com/Gerold55/banana_3ds) repositories.

Currently, development and releases of the Android version are in [a separate repository](https://gitlab.com/Gerold55/banana_3ds/citra-android).

### Development

Most of the development happens on GitHub. It's also where [our central repository](https://gitlab.com/Gerold55/banana_3ds) is hosted.

We centralize the translation work there, and periodically upstream translations.

### Building

* __Windows__: [Windows Build](https://github.com/citra-emu/citra/wiki/Building-For-Windows)
* __Linux__: [Linux Build](https://github.com/citra-emu/citra/wiki/Building-For-Linux)
* __macOS__: [macOS Build](https://github.com/citra-emu/citra/wiki/Building-for-macOS)